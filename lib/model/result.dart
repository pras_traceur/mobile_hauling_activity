part of 'models.dart';

class Result extends GetConnect {
  Future<Response> postAuthLogin(FormData body) =>
      post(apiBaseUrl + "/auth/login", body, contentType: "application/json");

  // Future<Response> getalldatastatus(String id_user) =>
  //     get(apiBaseUrl + "/user/alldatastatus/" + id_user,
  //         contentType: "application/json");

  Future<http.Response> getAllDataUser(String id_user) async {
    http.Response response =
        await http.get(apiBaseUrl + "/user/alldatastatus/" + id_user);
    return response;
  }

  Future<http.Response> getPenawaran(String truckId) async { //Panggil ketika idle
    http.Response response =
        await http.get(apiBaseUrl + "/hauling/getPenawaran/" + truckId);
    return response;
  }

  Future<http.Response> getHauling(String haulingId) async { //Panggil ketika pickup
    http.Response response =
        await http.get(apiBaseUrl + "/hauling/get/" + haulingId);
    return response;
  }

  Future<Response> postToken(FormData body) =>
      post(apiBaseUrl + "/pushnotif/sendtoken", body, contentType: "application/json");

  Future<Response> postAcceptHauling(FormData body) =>
      post(apiBaseUrl + "/hauling/accepttruck", body, contentType: "application/json"); // STEP 1 : accept

  Future<Response> postRejectHauling(FormData body) =>
      post(apiBaseUrl + "/hauling/settemptruck", body, contentType: "application/json"); // STEP 1 : accept

  Future<Response> postSetPoint(FormData body) =>
      post(apiBaseUrl + "/logpoint/create", body, contentType: "application/json"); // STEP 2 : set point

  Future<Response> postSetStatus(FormData body) =>
      post(apiBaseUrl + "/hauling/updatehaulingstatus", body, contentType: "application/json"); // STEP 3 : deket rom // STEP 5 : deket port

  Future<Response> postSetQuantity(FormData body) =>
      post(apiBaseUrl + "/hauling/setquantity", body, contentType: "application/json"); // STEP 4 : deket timbangan

      
}
