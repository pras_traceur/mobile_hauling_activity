import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_hauling_activity/pages/dashboard/dashboard_page.dart';
import 'package:mobile_hauling_activity/palatte.dart';
import 'package:mobile_hauling_activity/widgets/background_image.dart';
import 'login_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:localstorage/localstorage.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:another_flushbar/flushbar_helper.dart';
import 'package:another_flushbar/flushbar_route.dart';
import '../../model/models.dart';
import 'dart:io';
import 'dart:developer';

/// Define a top-level named handler which background/terminated messages will
/// call.
///
/// To verify things are working, check out the native platform logs.
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp(
      options: const FirebaseOptions(
    apiKey: 'AIzaSyDTsGMvrpp5yhhJIRK5R0_9ZYU96bDO7as',
    appId: '1:741558867475:android:e4932efa294e8ed724d1e6',
    messagingSenderId: '741558867475',
    projectId: 'sinarmas-mini',
  ));
  print('Handling a background message ${message.messageId}');
}

/// Create a [AndroidNotificationChannel] for heads up notifications
AndroidNotificationChannel channel;

/// Initialize the [FlutterLocalNotificationsPlugin] package.
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

class LoginPage extends GetView<LoginController> {
  final LocalStorage storage = new LocalStorage('mobile_hauling_activity');
  bool isUsernameValid = false;
  bool isPasswordValid = false;
  bool isSigninValid = false;

  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final Result result = Result();
  FirebaseMessaging firebaseMessaging;
  String token;
  Future<Function> getTokenFcm() async {
    await Firebase.initializeApp(
      options: const FirebaseOptions(
        apiKey: 'AIzaSyDTsGMvrpp5yhhJIRK5R0_9ZYU96bDO7as',
        appId: '1:741558867475:android:e4932efa294e8ed724d1e6',
        messagingSenderId: '741558867475',
        projectId: 'sinarmas-mini',
      ),
    ).whenComplete(() {
      print("completed Firebase.initializeApp");
    });
    // String token = await FirebaseMessaging.instance.getAPNSToken();
    // print('FlutterFire Messaging Example: Got APNs token: $token');

    token = await FirebaseMessaging.instance.getToken(
      vapidKey:
          "BA7UILU1bHQNa5j8x-ng9XnifoQQeXwksPhr74XTQoLod3QQHpxAA_fUP0Zyc6pDcpYnXKLb3TwQMGdpQdBz_60",
    );
    log('Data TOKEN: $token');

    // Set the background messaging handler early on, as a named top-level function
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    if (!kIsWeb) {
      channel = const AndroidNotificationChannel(
        'high_importance_channel', // id
        'High Importance Notifications', // title
        //'This channel is used for important notifications.', // description
        importance: Importance.high,
      );

      flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

      /// Create an Android Notification Channel.
      ///
      /// We use this channel in the `AndroidManifest.xml` file to override the
      /// default FCM channel to enable heads up notifications.
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(channel);

      /// Update the iOS foreground notification presentation options to allow
      /// heads up notifications.
      await FirebaseMessaging.instance
          .setForegroundNotificationPresentationOptions(
        alert: true,
        badge: true,
        sound: true,
      );
    }
  }

  Future<bool> login(String username, String password) async {
    FormData body = FormData({"username": username, "password": password});
    await result
        .postAuthLogin(body)
        .then((value) async {
          print(value.body);
          if (value.body['success'] == true) {
            storage.setItem('login', true);
            storage.setItem('id', value.body['user']['id']);
            // storage.setItem('token', value.body['token']);
            return true;
          } else {
            storage.setItem('login', false);
            return false;
          }
        })
        .timeout(Duration(seconds: 2))
        .catchError((error) async {
          await Future.delayed(Duration(seconds: 1));
        })
        .then((value) => () {
              getTokenFcm();
              FormData bodyToken =
                  FormData({"token": token, "truck_id": storage.getItem('id')});
              result.postToken(bodyToken).then((value) {
                log('sudah kirim token balikannya: ');
                print(value.body);
              });
            });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {},
      child: Stack(
        children: [
          BackgroundImage(),
          Scaffold(
            backgroundColor: Colors.transparent,
            body: SingleChildScrollView(
              child: SafeArea(
                child: Column(
                  children: [
                    Container(
                      height: 150,
                      child: Center(
                        child: Text(
                          'Hauling Activity',
                          style: kHeading,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Image(
                      image: AssetImage('assets/images/truck.png'),
                      width: 200,
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 40),
                      child: Column(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 12.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.grey[600]?.withOpacity(0.5),
                                    borderRadius: BorderRadius.circular(16),
                                  ),
                                  child: TextField(
                                    onChanged: (text) {
                                      isUsernameValid = text.length >= 1;
                                    },
                                    controller: usernameController,
                                    decoration: InputDecoration(
                                      contentPadding:
                                          const EdgeInsets.symmetric(
                                              vertical: 20),
                                      border: InputBorder.none,
                                      hintText: 'Username',
                                      prefixIcon: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: Icon(
                                          FontAwesomeIcons.user,
                                          color: Colors.white,
                                          size: 30,
                                        ),
                                      ),
                                      hintStyle: kBodyText,
                                    ),
                                    style: kBodyText,
                                    keyboardType: TextInputType.emailAddress,
                                    textInputAction: TextInputAction.next,
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 12.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.grey[600]?.withOpacity(0.5),
                                    borderRadius: BorderRadius.circular(16),
                                  ),
                                  child: TextField(
                                    onChanged: (text) {
                                      isPasswordValid = text.length > 5;
                                    },
                                    controller: passwordController,
                                    decoration: InputDecoration(
                                      contentPadding:
                                          const EdgeInsets.symmetric(
                                              vertical: 20),
                                      border: InputBorder.none,
                                      hintText: 'Password',
                                      prefixIcon: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: Icon(
                                          FontAwesomeIcons.lock,
                                          color: Colors.white,
                                          size: 30,
                                        ),
                                      ),
                                      hintStyle: kBodyText,
                                    ),
                                    obscureText: true,
                                    style: kBodyText,
                                    textInputAction: TextInputAction.done,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              SizedBox(
                                height: 50,
                              ),
                              Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  color: Colors.orange,
                                  borderRadius: BorderRadius.circular(16),
                                ),
                                child: FlatButton(
                                  onPressed: () {
                                    if (isUsernameValid && isPasswordValid) {
                                      login(usernameController.text,
                                              passwordController.text)
                                          .then((value) => {
                                                if (storage.getItem('login') ==
                                                    true)
                                                  {
                                                    Flushbar(
                                                      backgroundColor:
                                                          Colors.green,
                                                      flushbarPosition:
                                                          FlushbarPosition.TOP,
                                                      message:
                                                          "Selamat Datang ",
                                                      duration:
                                                          Duration(seconds: 3),
                                                    )..show(context),
                                                    // sleep(Duration(seconds: 3)),
                                                    Get.to(
                                                        () => DashboardPage(),
                                                        transition:
                                                            Transition.downToUp)
                                                  }
                                                else
                                                  {
                                                    Flushbar(
                                                      backgroundColor:
                                                          Colors.red,
                                                      flushbarPosition:
                                                          FlushbarPosition.TOP,
                                                      message:
                                                          "Pastikan Username/Password Sudah Benar !",
                                                      duration:
                                                          Duration(seconds: 3),
                                                    )..show(context)
                                                  }
                                              });
                                    } else {
                                      Flushbar(
                                        backgroundColor: Colors.red,
                                        flushbarPosition: FlushbarPosition.TOP,
                                        message:
                                            "Pastikan Username/Password Sudah terisi !",
                                        duration: Duration(seconds: 3),
                                      )..show(context);
                                    }
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 16.0),
                                    child: Text(
                                      "Login",
                                      style: kBodyText,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
