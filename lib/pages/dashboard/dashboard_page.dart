import 'dart:convert';
import 'dart:developer';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_hauling_activity/pages/account/account_page.dart';
import 'package:mobile_hauling_activity/pages/alerts/alerts_page.dart';
import 'package:mobile_hauling_activity/pages/home/home_controller.dart';
import 'package:mobile_hauling_activity/pages/home/home_page.dart';
import 'package:mobile_hauling_activity/pages/login/login_page.dart';
import 'package:localstorage/localstorage.dart';
import 'dart:async';
import 'dashboard_controller.dart';
import '../../model/models.dart';

/// Initialize the [FlutterLocalNotificationsPlugin] package.
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
Timer mytimer;

class DashboardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    HomeController homeController = new HomeController();
    final Result result = Result();
    final LocalStorage storage = new LocalStorage('mobile_hauling_activity');

    String id_user = storage.getItem('id').toString();

    if (storage.getItem('login') != true) {
      return LoginPage();
    }

    Future<void> GetData(String id_user) async {
      // id_user = "3";
      await result.getAllDataUser(id_user).then((value) async {
        // print(value.body);
        Map<String, dynamic> data = jsonDecode(value.body);

        if (data['success'] == true) {
          //user
          storage.setItem('username', data['user']['username']);
          storage.setItem('email', data['user']['email']);
          storage.setItem('role_id', data['user']['role_id']);
          //truck
          storage.setItem('truck_id', data['truck']['id']);
          storage.setItem('driver', data['truck']['driver']);
          storage.setItem('nomor_lambung', data['truck']['nomor_lambung']);
          storage.setItem('current_point', data['truck']['current_point']);
          storage.setItem('type', data['truck']['type']);
        } else {
          // print("gagal");
          storage.setItem('login', false);
          Get.to(() => LoginPage(), transition: Transition.upToDown);
        }
      }).timeout(Duration(seconds: 2));
    }

    mytimer?.cancel();
    mytimer = Timer.periodic(Duration(seconds: 2), (timer) {
      if (id_user != null) {
        GetData(id_user);
      }
    });

    @override
    void initState() {
      FirebaseMessaging.instance
          .getInitialMessage()
          .then((RemoteMessage message) {
        if (message != null) {
          print('A new getInitialMessage event was published! :' +
              message.data["hauling_id"]);
          log(message.data["hauling_id"]);
        }
      });

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        RemoteNotification notification = message.notification;
        log(message.data["hauling_id"]);
        AndroidNotification android = message.notification?.android;
        if (notification != null && android != null) {
          //&& !kIsWeb
          flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title + message.data["hauling_id"],
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                // TODO add a proper drawable resource to android, for now using
                //      one that already exists in example app.
                icon: 'launch_background',
              ),
            ),
          );
        }
      });

      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        print('A new onMessageOpenedApp event was published! : ' +
            message.data["hauling_id"]);
      });

      id_user = storage.getItem('id').toString();
      if (id_user != null) {
        GetData(id_user);
      }
    }

    return WillPopScope(
      onWillPop: () {},
      child: GetBuilder<DashboardController>(
        builder: (controller) {
          return Scaffold(
            body: SafeArea(
              child: IndexedStack(
                index: controller.tabIndex,
                children: [
                  HomePage(),
                  AccountPage(),
                ],
              ),
            ),
            bottomNavigationBar: BottomNavigationBar(
              unselectedItemColor: Colors.black,
              selectedItemColor: Colors.orange,
              onTap: controller.changeTabIndex,
              currentIndex: controller.tabIndex,
              showSelectedLabels: false,
              showUnselectedLabels: false,
              type: BottomNavigationBarType.fixed,
              backgroundColor: Colors.white,
              elevation: 0,
              items: [
                _bottomNavigationBarItem(
                  icon: CupertinoIcons.home,
                  label: 'Home',
                ),
                _bottomNavigationBarItem(
                  icon: CupertinoIcons.person,
                  label: 'Account',
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  _bottomNavigationBarItem(
      {IconData icon = CupertinoIcons.home, String label = 'Home'}) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      label: label,
    );
  }
}
