import 'package:get/get.dart';
import 'package:localstorage/localstorage.dart';
import '../../model/models.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:math';

class HomeController extends GetxController {
  final LocalStorage storage = new LocalStorage('mobile_hauling_activity');

  final Result result = Result();

  var current_point = "10".obs;
  var shift = "".obs;
  var driver = "".obs;
  var nomor_lambung = "".obs;
  var current_job = "N/A".obs;

  var latitude = 0.0.obs;
  var longitude = 0.0.obs;
  var distanceWeiger = 0.0.obs;
  var distancePort = 0.0.obs;
  var permisionStat = 'allow'.obs;
  var isLoading = false.obs;
  var haulingStat = "none".obs;
  var serviceEnabled = true.obs;
  var countDownOrder = 10.obs;
  var countDownDelivery = 1000.obs;
  var cdDelDetikMenitJam = "".obs;
  var startDate = "".obs;
  var statusText = "".obs;
  var statusImg = "".obs;
  var isSelesai = true.obs;

  var finalCDOrder = true.obs;
  var finalCDDelivery = true.obs;

  var hauling_id = "0".obs;

  void updateDate(date) {
    startDate.value = date;
  }

  void updateHaulingId(haulingId) {
    hauling_id.value = haulingId;
  }

  void updateSelesai(selesai) {
    isSelesai.value = selesai;
  }

  void updateStat(status) {
    haulingStat.value = status;
    if (status == "pick up") {
      statusText.value = "Menuju ke ROM (Tambang)";
      statusImg.value = "assets/images/rom.png";
    } else if (status == "delivery") {
      statusText.value = "Menuju ke Weiger & PORT";
      statusImg.value = "assets/images/port.png";
    }
  }

  void shiftCounter() {
    DateTime now = new DateTime.now();
    if (now.hour >= 7 && now.hour < 19) {
      shift.value = "Morning";
    } else {
      shift.value = "Night";
    }
  }

  void PointCounter(point) {
    current_point.value = point;
  }

  void DriverCounter() {
    driver.value = storage.getItem('driver');
  }

  void NomorLambungCounter() {
    nomor_lambung.value = storage.getItem('nomor_lambung');
  }

  void JobCounter(job) {
    current_job.value = job;
    // current_job.value = "N/A";
  }

  void CountDownDelivery(estimate_time) {
    countDownDelivery.value = estimate_time;
  }

  void CountDownOrderCus() {
    if (!finalCDOrder.value) {
      countDownOrder.value = countDownOrder.value - 1;
      if (countDownOrder.value < 0) {
        finalCDOrder.value = true;
        countDownOrder.value = 0;
        setPoint("reject", storage.getItem("hauling_id"), -0.5,
            storage.getItem("truck_id"), "nothing response");
        rejectHauling(
            storage.getItem("hauling_id"), storage.getItem("truck_id"));
        updateStat("none");
      }
    }
  }

  void startCounterOrder() {
    countDownOrder.value = 10;
    finalCDOrder.value = false;
  }

  void stopCounterOrder() {
    finalCDOrder.value = true;
  }

  void resetCountDown() {
    stopCounterOrder();
    // countDownOrder.value = 10;
    stopCountDownDelivery();
    // countDownDelivery.value = 1000;
  }

  void CountDownDeliveryCus() {
    if (!finalCDDelivery.value) {
      countDownDelivery.value = countDownDelivery.value - 1;
      if (countDownDelivery.value < 0) {
        finalCDDelivery.value = true;
        countDownDelivery.value = 0;
      }
    }

    cdDelDetikMenitJam.value = intToTimeLeft(countDownDelivery.value);
  }

  void startCountDownDelivery() {
    // countDownDelivery.value = 1000; //tunggu kiriman
    finalCDDelivery.value = false;
  }

  void stopCountDownDelivery() {
    finalCDDelivery.value = true;
  }

  String intToTimeLeft(int value) {
    int h, m, s;

    h = value ~/ 3600;
    m = ((value - h * 3600)) ~/ 60;
    s = value - (h * 3600) - (m * 60);

    String hourLeft =
        h.toString().length < 2 ? "0" + h.toString() : h.toString();
    String minuteLeft =
        m.toString().length < 2 ? "0" + m.toString() : m.toString();
    String secondsLeft =
        s.toString().length < 2 ? "0" + s.toString() : s.toString();

    String result = "$hourLeft:$minuteLeft:$secondsLeft";
    return result;
  }

  void acceptHauling(haulingId, truckId) async {
    FormData body = FormData({"id": haulingId, "truck_id": truckId});
    await result
        .postAcceptHauling(body)
        .then((value) async {
          setPoint("accept", haulingId, 1, truckId, "terima");
        })
        .timeout(Duration(seconds: 2))
        .catchError((error) async {
          await Future.delayed(Duration(seconds: 1));
        });
  }

  void rejectHauling(haulingId, truckId) async {
    FormData body = FormData({"id": haulingId, "truck_id": 0});
    await result
        .postRejectHauling(body)
        .then((value) async {
          setPoint("reject", haulingId, -0.5, truckId, "tolak");
        })
        .timeout(Duration(seconds: 2))
        .catchError((error) async {
          await Future.delayed(Duration(seconds: 1));
        });
  }

  void setPoint(typePoint, haulingId, point, truckId, String reason) async {
    FormData body = FormData({
      "type": typePoint,
      "hauling_id": haulingId,
      "point": point,
      "truck_id": truckId,
      "reason": reason
    });
    await result
        .postSetPoint(body)
        .then((value) async {
          print(value.body);
        })
        .timeout(Duration(seconds: 2))
        .catchError((error) async {
          await Future.delayed(Duration(seconds: 1));
        });
  }

  void setStatus(int haulingId, String statusHauling) async {
    FormData body = FormData({"id": haulingId, "status": statusHauling});
    await result
        .postSetStatus(body)
        .then((value) async {
          print(value.body);
        })
        .timeout(Duration(seconds: 2))
        .catchError((error) async {
          await Future.delayed(Duration(seconds: 1));
        });
  }

  void setQuantity(int haulingId) async {
    FormData body = FormData({"id": haulingId});
    await result
        .postSetQuantity(body)
        .then((value) async {
          print(value.body);
        })
        .timeout(Duration(seconds: 2))
        .catchError((error) async {
          await Future.delayed(Duration(seconds: 1));
        });
  }

  void _reset() {
    latitude.value = 0.0;
    longitude.value = 0.0;
    serviceEnabled.value = true;
    permisionStat.value = 'allow';
  }

  Future<Position> _determinePosition() async {
    Position position = Position(
        longitude: 0.0,
        latitude: 0.0,
        timestamp: DateTime.now(),
        accuracy: 0,
        altitude: 0,
        heading: 0,
        speed: 0,
        speedAccuracy: 0);
    await Permission.location.request();
    if (await Permission.location.request().isGranted) {
      LocationPermission permission;
      serviceEnabled.value = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled.value) {
        return Future.error('message-error-location-service-off'.tr);
      }
      await Permission.location.request();
      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied ||
          permission == LocationPermission.deniedForever) {
        permisionStat.value = 'denied';
        await Permission.location.request();
        permission = await Geolocator.checkPermission();
        if (permission == LocationPermission.deniedForever) {
          permisionStat.value = 'deniedForever';
          return Future.error('message-error-location-denied-forever'.tr);
        }
        if (permission == LocationPermission.denied) {
          await Permission.location.request();
          permisionStat.value = 'denied';
          return Future.error('message-error-location-denied'.tr);
        }
      }
      permisionStat.value = 'allow';
      position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      if (position.isMocked) {
        return Future.error('message-error-location-mocked'.tr);
      }
    } else {
      await Permission.location.request();
      return Future.error('message-error-location-denied-forever'.tr);
    }
    return position;
  }

  Future<void> _getLocation() async {
    await _determinePosition().then((position) async {
      latitude.value = position.latitude;
      longitude.value = position.longitude;
    }).catchError((e) {
      latitude.value = 0.0;
      longitude.value = 0.0;
    }).onError((e, stackTrace) {
      latitude.value = 0.0;
      longitude.value = 0.0;
    });
  }

  double _calculateDistance(lat1, lon1, lat2, lon2) {
    return Geolocator.distanceBetween(lat1, lon1, lat2, lon2);
  }

  Future<void> getDistanceWeiger(lat, long) async {
    // _reset();
    await _getLocation();
    await Future.delayed(Duration(
      milliseconds: 500,
    ));
    double distance = 0.0;
    bool status = false;
    int i = 0;
    distance = _calculateDistance(latitude.value, longitude.value, lat, long);
    distanceWeiger.value = distance;
  }

  Future<void> getDistancePort(lat, long) async {
    // _reset();
    await _getLocation();
    await Future.delayed(Duration(
      milliseconds: 500,
    ));
    double distance = 0.0;
    bool status = false;
    int i = 0;
    distance = _calculateDistance(latitude.value, longitude.value, lat, long);
    distancePort.value = distance;
  }

  int getRandom() {
    Random rnd;
    int min = 20;
    int max = 30;
    rnd = new Random();
    int r = min + rnd.nextInt(max - min);
    return r;
  }
}
