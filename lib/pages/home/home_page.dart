import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:mobile_hauling_activity/pages/alerts/alerts_page.dart';
import 'package:mobile_hauling_activity/pages/dashboard/dashboard_page.dart';
import 'home_controller.dart';
import '../../palatte.dart';
import '../../widgets/widgets.dart';
import 'package:mobile_hauling_activity/model/message_item.dart';
import 'package:mobile_hauling_activity/model/notification_data.dart';
import 'dart:async';
import '../../model/models.dart';
import 'dart:convert';
import 'package:localstorage/localstorage.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:overlay_support/overlay_support.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
Timer mytimer, mytimer2;

class HomePage extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    final Result result = Result();
    final LocalStorage storage = new LocalStorage('mobile_hauling_activity');
    String id_truck = storage.getItem('truck_id').toString();
    String hauling_id = storage.getItem('truck_id').toString();
    String number = storage.getItem('number').toString();
    String point = storage.getItem('current_point').toString();

    // Future<void> _showNotification() async {
    //   flutterLocalNotificationsPlugin.show(
    //     1,
    //     'New Order ' + storage.getItem("number"),
    //     'Anda mendapatkan order ${storage.getItem("number")}, masuk aplikasi untuk melihat !',
    //     NotificationDetails(
    //       android: AndroidNotificationDetails(
    //         "hauling",
    //         "sinarmas",
    //         icon: 'launch_background',
    //       ),
    //     ),
    //   );
    // }

    Future<void> GetHaulingTemp(String id_truck) async {
      await result.getPenawaran(id_truck).then((value) async {
        // print(value.body);
        // print(id_truck);
        Map<String, dynamic> data = jsonDecode(value.body);
        if (data['success'] == true) {
          controller.updateHaulingId(data['id']);
          controller.updateStat("order");
          controller.JobCounter(data['data']['number']);
          controller.startCounterOrder();
          storage.setItem('hauling_id', data['data']['id']);
          // await _showNotification();
          showSimpleNotification(
            Text("Anda Menerima Order Baru !"),
            background: Colors.purple,
            autoDismiss: false,
            trailing: Builder(builder: (context) {
              return FlatButton(
                  textColor: Colors.yellow,
                  onPressed: () {
                    OverlaySupportEntry.of(context).dismiss();
                  },
                  child: Text('Dismiss'));
            }),
          );
        } else {
          controller.JobCounter("N/A");
          controller.updateStat("none");
        }
      }).timeout(Duration(seconds: 2));
    }

    Future<void> GetHauling(String hauling_id) async {
      await result.getHauling(hauling_id).then((value) async {
        // print(value.body);
        // print(id_truck);
        Map<String, dynamic> data = jsonDecode(value.body);
        if (data['success'] == true) {
          controller.updateStat(data['data']['status']);
          controller.JobCounter(data['data']['number']);
          controller.updateDate(data['data']['start_date']);

          if (data['data']['status'] == "pick up") {
            controller.CountDownDelivery((data['data']['estimate_time'] * 60));
          } else if (data['data']['status'] == "delivery") {
            controller.startCountDownDelivery();
          } else if (data['data']['status'] == "done") {
            controller.stopCountDownDelivery();
          }
        } else {
          controller.JobCounter("N/A");
          // controller.updateStat("none");
        }
      }).timeout(Duration(seconds: 2));
    }

    Future<void> _reCheck() async {
      await controller.getDistanceWeiger(-6.076123, 106.445848);
      await controller.getDistancePort(-6.034441, 106.577103);
    }

    void UpdateDataUser() {
      controller.shiftCounter();
      controller.DriverCounter();
      controller.NomorLambungCounter();
      _reCheck();
      id_truck = storage.getItem('truck_id').toString();
      hauling_id = storage.getItem('hauling_id').toString();
      point = storage.getItem('current_point').toString();
      controller.PointCounter(point);
      if (id_truck != null) {
        // if (hauling_id != null) {
        //   GetHauling(hauling_id);
        // } else {
        if (controller.haulingStat.value == "none") {
          GetHaulingTemp(id_truck);
        } else if (controller.haulingStat.value == "order") {
        } else {
          GetHauling(hauling_id);
        }
        // }
      }
    }

    mytimer?.cancel();
    mytimer2?.cancel();
    mytimer = Timer.periodic(Duration(seconds: 2), (timer) {
      UpdateDataUser();
    });

    mytimer2 = Timer.periodic(Duration(seconds: 1), (timer) {
      // print("random");
      // print(controller.getRandom());
      controller.CountDownOrderCus();
      controller.CountDownDeliveryCus();
      if (controller.countDownOrder.value <= 0 ||
          controller.countDownDelivery.value <= 0) {
        controller.resetCountDown();
        controller.updateSelesai(false);
      }
    });

    return Scaffold(
      body: CustomScrollView(
        physics: ClampingScrollPhysics(),
        slivers: <Widget>[
          SliverPadding(
            padding: const EdgeInsets.all(20.0),
            sliver: SliverToBoxAdapter(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Welcome Back',
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Obx(
                    () => Text(
                      'Driver Truck ${controller.driver.value} ${controller.nomor_lambung.value}',
                      style: const TextStyle(
                        color: Colors.grey,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ])),
          ),
          SliverPadding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            sliver: SliverToBoxAdapter(
              child: Container(
                height: MediaQuery.of(context).size.height * 0.2,
                child: Column(
                  children: <Widget>[
                    Flexible(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              margin: const EdgeInsets.all(5.0),
                              padding: const EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                color: Colors.orange,
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Current Shift',
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 13.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  Obx(
                                    () => Text(
                                      '${controller.shift.value}',
                                      style: const TextStyle(
                                        color: Colors.white,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              margin: const EdgeInsets.all(5.0),
                              padding: const EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Current Point Achievement',
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 13.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  Obx(
                                    () => Text(
                                      '${controller.current_point.value}',
                                      style: const TextStyle(
                                        color: Colors.white,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Flexible(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              margin: const EdgeInsets.all(5.0),
                              padding: const EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                color: Colors.lightBlue,
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Current Job Assignment',
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 13.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  Obx(
                                    () => Text(
                                      '${controller.current_job.value}',
                                      style: const TextStyle(
                                        color: Colors.white,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SliverPadding(
            padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
            sliver: SliverToBoxAdapter(
              child: Obx(
                () => (controller.haulingStat.value == "none")
                    ? Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(8.0),
                          padding: const EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 10), // changes position of shadow
                              ),
                            ],
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(height: 60),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Container(
                                    child: Image.asset(
                                        "assets/images/mining.png",
                                        height: 150),
                                  )
                                ],
                              ),
                              SizedBox(height: 20),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      "Belum ada Permintaan Baru",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 60),
                            ],
                          ),
                        ),
                      )
                    : (controller.haulingStat.value == "order")
                        ? Expanded(
                            child: Container(
                              margin: const EdgeInsets.all(8.0),
                              padding: const EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10.0),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.1),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 10), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Order",
                                    style: const TextStyle(
                                      color: Colors.grey,
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Obx(
                                    () => Text(
                                      "${controller.startDate.value}",
                                      style: const TextStyle(
                                        color: Colors.grey,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  // Text(
                                  //   "Tambang 1 -> Port 1",
                                  //   style: const TextStyle(
                                  //     color: Colors.black,
                                  //     fontSize: 20.0,
                                  //     fontWeight: FontWeight.bold,
                                  //   ),
                                  // ),
                                  SizedBox(height: 50),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Container(
                                        child: Text(
                                          "You have new Order !",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 10),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Obx(
                                        () => Text(
                                          "CountDown : ${controller.countDownOrder.value} detik",
                                          style: const TextStyle(
                                              color: Colors.orange,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 10),
                                  SizedBox(height: 20),
                                  Container(
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.circular(16),
                                    ),
                                    child: FlatButton(
                                      onPressed: () {
                                        controller.acceptHauling(
                                            storage.getItem("hauling_id"),
                                            storage.getItem("truck_id"));
                                        controller.updateStat("pick up");
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 16.0),
                                        child: Text(
                                          "Accept",
                                          style: kBodyText,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Container(
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.circular(16),
                                    ),
                                    child: FlatButton(
                                      onPressed: () {
                                        controller.setPoint(
                                            "reject",
                                            storage.getItem("hauling_id"),
                                            -0.5,
                                            storage.getItem("truck_id"),
                                            "sedang sibuk");
                                        controller.rejectHauling(
                                            storage.getItem("hauling_id"),
                                            storage.getItem("truck_id"));
                                        controller.updateStat("none");
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 16.0),
                                        child: Text(
                                          "Reject",
                                          style: kBodyText,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Expanded(
                            child: Container(
                              margin: const EdgeInsets.all(8.0),
                              padding: const EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10.0),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.1),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 10), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Order",
                                    style: const TextStyle(
                                      color: Colors.grey,
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Obx(
                                    () => Text(
                                      "${controller.startDate.value}",
                                      style: const TextStyle(
                                        color: Colors.grey,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  // Text(
                                  //   "Tambang 1 -> Port 1",
                                  //   style: const TextStyle(
                                  //     color: Colors.black,
                                  //     fontSize: 20.0,
                                  //     fontWeight: FontWeight.bold,
                                  //   ),
                                  // ),
                                  SizedBox(height: 20),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Container(
                                        child: Obx(
                                          () => Image.asset(
                                              "${controller.statusImg.value}",
                                              height: 100),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 20),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Container(
                                        child: Obx(
                                          () => Text(
                                            "${controller.statusText.value}",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 10),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Obx(
                                        () => Text(
                                          'CountDown : ${controller.cdDelDetikMenitJam.value}',
                                          style: const TextStyle(
                                              color: Colors.orange,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 5),
                                  // Row(
                                  //   mainAxisAlignment: MainAxisAlignment.center,
                                  //   mainAxisSize: MainAxisSize.max,
                                  //   children: <Widget>[
                                  //     Obx(
                                  //       () => Text(
                                  //         'Distance Weiger : ${controller.distanceWeiger.value.toInt()} - Distance Port : ${controller.distancePort.value.toInt()}',
                                  //         style: const TextStyle(
                                  //           color: Colors.grey,
                                  //           fontSize: 10.0,
                                  //           fontWeight: FontWeight.bold,
                                  //         ),
                                  //       ),
                                  //     ),
                                  //   ],
                                  // ),
                                  // SizedBox(height: 5),

                                  // Row(
                                  //   mainAxisAlignment: MainAxisAlignment.center,
                                  //   mainAxisSize: MainAxisSize.max,
                                  //   children: <Widget>[
                                  //     Obx(
                                  //       () => Text(
                                  //         'Lat - long : ${controller.latitude.value} - ${controller.longitude.value}',
                                  //         style: const TextStyle(
                                  //           color: Colors.grey,
                                  //           fontSize: 10.0,
                                  //           fontWeight: FontWeight.bold,
                                  //         ),
                                  //       ),
                                  //     ),
                                  //   ],
                                  // ),
                                  SizedBox(height: 10),
                                  Obx(
                                    () => (controller.haulingStat.value ==
                                            "delivery")
                                        ? Container(
                                            width: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Colors.green,
                                              borderRadius:
                                                  BorderRadius.circular(16),
                                            ),
                                            child: FlatButton(
                                              onPressed: () {
                                                controller.setStatus(
                                                    storage
                                                        .getItem("hauling_id"),
                                                    "done");

                                                // controller.setQuantity(storage
                                                //     .getItem("hauling_id"));

                                                if (controller.countDownDelivery
                                                        .value ==
                                                    0) {
                                                  controller.setPoint(
                                                      "done",
                                                      storage.getItem(
                                                          "hauling_id"),
                                                      1,
                                                      storage
                                                          .getItem("truck_id"),
                                                      "finish late");
                                                } else {
                                                  controller.setPoint(
                                                      "done",
                                                      storage.getItem(
                                                          "hauling_id"),
                                                      2,
                                                      storage
                                                          .getItem("truck_id"),
                                                      "finish ontime");
                                                }
                                                controller.updateStat("none");
                                              },
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 16.0),
                                                child: Text(
                                                  "Selesai",
                                                  style: kBodyText,
                                                ),
                                              ),
                                            ),
                                          )
                                        : SizedBox(height: 30),
                                  ),
                                ],
                              ),
                            ),
                          ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
