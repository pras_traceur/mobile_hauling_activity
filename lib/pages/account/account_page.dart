import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../login/login_page.dart';
import 'account_controller.dart';
import '../../palatte.dart';
import 'package:localstorage/localstorage.dart';

class AccountPage extends GetView<AccountController> {
  @override
  Widget build(BuildContext context) {
    final LocalStorage storage = new LocalStorage('mobile_hauling_activity');
    TextEditingController emailController = TextEditingController();
    TextEditingController usernameController = TextEditingController();
    TextEditingController nomorlambungController = TextEditingController();

    emailController.text = storage.getItem("email");
    usernameController.text = storage.getItem("username");
    nomorlambungController.text = storage.getItem("nomor_lambung");

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Account",
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image(
                image: AssetImage('assets/images/truck.png'),
                width: 200,
              ),
              SizedBox(height: 10),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 30),
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: TextField(
                    enabled: false,
                    onChanged: (text) {},
                    controller: emailController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Email"),
                  ),
                ),
              ),
              SizedBox(height: 15),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 30),
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: TextField(
                    enabled: false,
                    onChanged: (text) {},
                    controller: usernameController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Username"),
                  ),
                ),
              ),
              SizedBox(height: 15),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 30),
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: TextField(
                    enabled: false,
                    onChanged: (text) {},
                    controller: nomorlambungController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Truk"),
                  ),
                ),
              ),

              SizedBox(height: 80),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 30),
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: FlatButton(
                    onPressed: () {
                      storage.clear();
                      storage.setItem('login', false);
                      Get.to(() => LoginPage(),
                          transition: Transition.upToDown);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: Text(
                        "LogOut",
                        style: kBodyText,
                      ),
                    ),
                  ),
                ),
              ),
              // Obx(() => Text("Counter ${controller.counter.value}")),
              // RaisedButton(
              //   child: Text("Increase"),
              //   onPressed: () => controller.increaseCounter(),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
